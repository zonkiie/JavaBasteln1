package dynamic_classgeneration;

import java.beans.beancontext.*;
import java.lang.*;
import java.lang.reflect.*;
import java.util.*;
import java.io.*;
import net.sf.cglib.beans.*;
import net.sf.cglib.core.*;
import org.apache.commons.lang3.builder.*;
import javassist.*;
import javassist.bytecode.*;
import javassist.bytecode.annotation.*;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author rainer
 */
public class ClassGenerator {

    public static void SetExceptionHandler() {
        // @see http://www.nomachetejuggling.com/2006/06/13/java-5-global-exception-handling/
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

            @Override
            public void uncaughtException(Thread t, Throwable e) {
                StringWriter writer = new StringWriter();
                writer.append(e.getMessage() + "\n");
                e.printStackTrace(new PrintWriter(writer));
                System.err.println("Exception thrown:" + writer.toString());

            }
        });

    }

    /**
     * @see
     * https://stackoverflow.com/questions/5178391/create-simple-pojo-classes-bytecode-at-runtime-dynamically
     * @param className
     * @param properties
     * @return
     */
    public static Class<?> createBeanClass(
            /* fully qualified class name */
            final String className,
            /* bean properties, name -> type */
            final Map<String, Class<?>> properties) {

        final BeanGenerator beanGenerator = new BeanGenerator();

        /* use our own hard coded class name instead of a real naming policy */
        beanGenerator.setNamingPolicy(new NamingPolicy() {
            @Override
            public String getClassName(final String prefix,
                    final String source, final Object key, final Predicate names) {
                return className;
            }
        });
        BeanGenerator.addProperties(beanGenerator, properties);
        return (Class<?>) beanGenerator.createClass();
    }

    public static void TestCreateBeanClass() {
        // How to directly initialize a HashMap (in a literal way)?
        // @see https://stackoverflow.com/questions/6802483/how-to-directly-initialize-a-hashmap-in-a-literal-way
        try {
            Class<?> myclass = createBeanClass("Cl1", new HashMap<String, Class<?>>() {
                {
                    put("ID", UUID.class);
                    put("Name", String.class);
                    put("Friend", String.class);
                }
            });
            System.out.println(myclass);
            for (final Method method : myclass.getDeclaredMethods()) {
                System.out.println(method);
            }
            Object obj = myclass.newInstance();
            obj.getClass().getDeclaredMethod("setID", java.util.UUID.class).invoke(obj, UUID.randomUUID());
            obj.getClass().getDeclaredMethod("setName", java.lang.String.class).invoke(obj, "Bugs Bunny");
            obj.getClass().getDeclaredMethod("setFriend", java.lang.String.class).invoke(obj, "Duffy Duck");
            for (Field f : obj.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                System.out.println(f.getName() + "=" + f.get(obj));
            }
            System.out.println("ID:" + obj.getClass().getDeclaredMethod("getID").invoke(obj));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static Class<?> CreateJavassistClass(String Name, Map<String, Class<?>> properties) throws Exception {
        ClassPool pool = ClassPool.getDefault();
        CtClass cc = pool.makeClass(Name);
        List<String> ToStringFragments = new ArrayList<>();
        for (Map.Entry<String, Class<?>> property : properties.entrySet()) {
            Class<?> clazz = property.getValue();
            StringBuilder FieldModStr = new StringBuilder();
            FieldModStr.append(property.getKey().substring(0, 1).toUpperCase()).append(property.getKey().substring(1));
            CtField newField = new CtField(ClassPool.getDefault().get(clazz.getCanonicalName()), property.getKey(), cc);
            newField.setModifiers(java.lang.reflect.Modifier.PRIVATE);
            cc.addField(newField);
            //cc.addField(CtField.make("private " + clazz.getCanonicalName() + " " + property.getKey() + ";", cc));
            CtMethod getter = CtNewMethod.make("public " + property.getValue().getCanonicalName() + " get" + FieldModStr.toString() + "() { return this." + property.getKey() + ";}", cc);
            CtMethod setter = CtNewMethod.make("public " + Name + " set" + FieldModStr.toString() + "(" + property.getValue().getCanonicalName() + " data) { this." + property.getKey() + " = data; return this;}", cc);
            cc.addMethod(setter);
            cc.addMethod(getter);
            ToStringFragments.add("\"[" + property.getKey() + "=\"+" + property.getKey() + "+\"]\"");
        }
        String StrToStringFragments = "public String toString() { return \"(String):\"+" + StringUtils.join(ToStringFragments, "+\",\"+") + "; }";
        CtMethod toStringMethod = CtMethod.make(StrToStringFragments, cc);
        cc.addMethod(toStringMethod);
        cc.rebuildClassFile();
        return cc.toClass();
    }

    public static void TestCreateJavassistClass() throws Exception {
        Class<?> myclass = CreateJavassistClass("JACl1", new HashMap<String, Class<?>>() {
            {
                put("ID", UUID.class);
                put("Name", String.class);
                put("Friend", String.class);
            }
        });

        Object obj = myclass.newInstance();
        for (final Method method : myclass.getDeclaredMethods()) {
            System.out.println(method);
        }
        obj.getClass().getDeclaredMethod("setID", UUID.class).invoke(obj, UUID.randomUUID());
        obj.getClass().getDeclaredMethod("setName", String.class).invoke(obj, "Bugs Bunny");
        obj.getClass().getDeclaredMethod("setFriend", String.class).invoke(obj, "Duffy Duck");
        for (Field f : obj.getClass().getDeclaredFields()) {
            f.setAccessible(true);
             System.out.println(f + ":"  + f.getName() + "=" + f.get(obj));
        }
        System.out.println("Object:" + obj);
        System.out.println(ReflectionToStringBuilder.reflectionToString(obj));
    }

    public static void main(String[] args) throws Exception {
        SetExceptionHandler();
        //TestCreateBeanClass();
        TestCreateJavassistClass();
    }
}
