package janino1;

import java.util.*;
import java.lang.*;
import java.io.*;
import java.lang.reflect.*;
import java.net.URL;
import org.codehaus.janino.*;

/**
 *
 * @author rainer
 */
public class Janino1 {

    private String srcDir = null;

    public Janino1 setSrcDir(String srcDir) {
        this.srcDir = srcDir;
        return this;
    }

    public String getSrcDir() {
        return this.srcDir;
    }

    public static Janino1 newInstance() {
        return new Janino1();
    }

    /**
     * @param Classname The class to load. The class may not contain "/" and may
     * not end with ".java"
     */
    public Object LoadSourceClass(String Classname) {
        try {
            ClassLoader cl = new JavaSourceClassLoader(
                    this.getClass().getClassLoader(), // parentClassLoader
                    new File[]{new File(srcDir)}, // optionalSourcePath
                    (String) null // optionalCharacterEncoding
            );
            Object o = cl.loadClass(Classname).newInstance();
            if (o != null) {
                return o;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void ExecuteMethod(Object o, String Methodname) {
        try {
            Method m = o.getClass().getMethod(Methodname);
            m.invoke(o);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public List GetClassesFromClassLoader(ClassLoader cl) {
        List<String> Classes = new ArrayList<String>();
        try {
            Field f = cl.getClass().getDeclaredField("classes");
            f.setAccessible(true);
            Object classes = f.get(cl);
            if (classes instanceof Map) {
                for (Map.Entry<String, Object> entry : ((Map<String, Object>) classes).entrySet()) {
                    //System.out.println(entry.getKey() + "/" + entry.getValue());
                    //System.out.println("Entry:" + entry.getKey());
                    Classes.add(entry.getKey());
                }
            } else if (classes instanceof Collection) {
                for(Object entry: (Collection)classes) Classes.add(entry.toString());
            }
            f.setAccessible(false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Classes;
    }

    public void ListMethods(Class cl) {
        for (Method m : cl.getMethods()) {
            if (m.getName() != "main") {
                continue;
            }
            System.out.print("Method:" + m.getName() + ", Params:");
            for (Class param_class : m.getParameterTypes()) {
                System.out.print(param_class.getName());
            }
            System.out.print("Modifiers:" + Modifier.toString(m.getModifiers()) + ", Return type:" + m.getReturnType().getName());
            System.out.println();
        }

    }

    public void ExecuteProgram(String Filename) {
        try {
            SimpleCompiler sc = new SimpleCompiler(srcDir + "/" + Filename);

            List<String> classList = GetClassesFromClassLoader(sc.getClassLoader());
            System.out.println(classList);
            for (String classname : classList) {
                Class cl = Class.forName(classname, false, sc.getClassLoader());
                for (Method m : cl.getMethods()) {
                    if (!m.getName().equals("main")) {
                        continue;
                    }
                    if (m.getParameterTypes().length == 0) {
                        continue;
                    }
                    Method method = null;
                    try {
                        method = cl.getMethod("main", String[].class);
                    } catch (Exception ex) {
                    }
                    if (method != null) {
                        if (Modifier.isPublic(cl.getModifiers()) && Modifier.isPublic(m.getModifiers()) && Modifier.isStatic(m.getModifiers()) && m.getName().equals("main")) {
                            method.invoke(cl, new String[]{null});
                        }
                    }
                }
            }
            //Class clazz = sc.getClassLoader().loadClass("example1.example1");
            //System.out.println(clazz.getCanonicalName());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

	public void EvalProgram(String Content) {
		try
		{
			SimpleCompiler sc = new SimpleCompiler();
			StringReader sr = new StringReader(Content);
			sc.cook(sr);
			
			List<String> classList = GetClassesFromClassLoader(sc.getClassLoader());
			for(String classname: classList)
			{
				Class cl = Class.forName(classname, false, sc.getClassLoader());
				for(Method m:cl.getMethods())
				{
					if(!m.getName().equals("main")) continue;
					if(m.getParameterTypes().length == 0) continue;
					Method method = null;
					try { method = cl.getMethod("main", String[].class); }
					catch(Exception ex) {}
					if(method != null) if(Modifier.isPublic(cl.getModifiers()) && Modifier.isPublic(m.getModifiers()) && Modifier.isStatic(m.getModifiers()) && m.getName().equals("main")) method.invoke(cl, new String[]{null});
				}
				
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
    

    public static void main(String[] args) {
        Janino1 j = Janino1.newInstance().setSrcDir(System.getProperty("user.home") + "/java-janino");
        Object o = j.LoadSourceClass("example1.example1");
        j.ExecuteMethod(o, "execute");
        j.ExecuteProgram("example1/example1.java");
    }
}
