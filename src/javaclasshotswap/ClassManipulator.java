/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javaclasshotswap;

import javassist.*;

/**
 *
 * @author rainer
 */
public class ClassManipulator {
    private CtClass newClass;
    
    public static ClassManipulator newInstance()
    {
        return new ClassManipulator();
    }
    
    public ClassManipulator setNewClass(CtClass newClass)
    {
        this.newClass = newClass;
        return this;
    }
    
    public CtClass getNewClass()
    {
        return newClass;
    }
    
    public Class getGeneratedClass() throws CannotCompileException
    {
        return newClass.toClass();
    }

    public ClassManipulator AddField(Class clazz, String Field, Class FieldType) throws NotFoundException, CannotCompileException
    {
        ClassPool pool = ClassPool.getDefault();
        Loader cl = new Loader(pool);

        CtClass ct = pool.get(DataClass.class.getCanonicalName());
        CtClass CtClass = ClassPool.getDefault().getCtClass(FieldType.getCanonicalName());
        CtField CtField = new CtField(ClassPool.getDefault().getCtClass(FieldType.getCanonicalName()), Field, ct);
        ct.addField(CtField);
        ct.rebuildClassFile();
        return this;
    }
    
    public ClassManipulator CreateNewClass(Class clazz, String NewClassName) throws CannotCompileException, NotFoundException
    {
        newClass = ClassPool.getDefault().makeClass(NewClassName);
        newClass.setSuperclass(ClassPool.getDefault().getCtClass(clazz.getCanonicalName()));
        return this;
    }
    
    public ClassManipulator CreateNewClass(String NewClassName) throws CannotCompileException, NotFoundException
    {
        newClass = ClassPool.getDefault().makeClass(NewClassName);
        return this;
    }
    
    public ClassManipulator AddField(String FieldName, Class FieldType) throws NotFoundException, CannotCompileException
    {
        return AddField(FieldName, FieldType, null);
    }
    
    public ClassManipulator AddField(String FieldName, Class FieldType, Integer modifier) throws NotFoundException, CannotCompileException
    {
        CtField cf = new CtField(ClassPool.getDefault().getCtClass(FieldType.getCanonicalName()), FieldName, newClass);
        if(modifier == null) cf.setModifiers(cf.getModifiers() | Modifier.PUBLIC);
        else cf.setModifiers(modifier);
        newClass.addField(cf);
        return this;
    }
    
    
}
