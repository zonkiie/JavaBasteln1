/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javaclasshotswap;

import javassist.*;

/**
 *
 * @author rainer
 */
public class MyClassLoader extends ClassLoader
{
    byte[] classByteCode = null;
    
    public static MyClassLoader newInstance()
    {
        return new MyClassLoader();
    }
    
    public byte[] GetClassByteCode()
    {
        return classByteCode;
    }

    public MyClassLoader SetClassByteCode(byte[] classByteCode)
    {
        this.classByteCode = classByteCode;
        return this;
    }
    
    public Class findClass(String name) {
        return defineClass(name, classByteCode, 0, classByteCode.length);
    }

}
