/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javaclasshotswap;

import java.beans.Beans;
import java.io.IOException;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import javassist.*;

/**
 *
 * @author rainer
 * @link https://github.com/jboss-javassist/javassist/blob/3.18/sample/hotswap/Test.java
 * @link http://stackoverflow.com/questions/3174436/load-java-byte-code-at-runtime
 */
public class JavaClassHotswap {
    
    /**
     * @param args the command line arguments
     */
    public static void main2(String[] args) throws NotFoundException, CannotCompileException, ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        ClassPool pool = ClassPool.getDefault();
        Loader cl = new Loader(pool);

        CtClass ct = pool.get(DataClass.class.getCanonicalName());
        CtClass CtStringClass = ClassPool.getDefault().getCtClass(String.class.getCanonicalName());
        CtField CtGuguField = new CtField(ClassPool.getDefault().getCtClass(String.class.getCanonicalName()), "Gugu", ct);
        ct.addField(CtGuguField);
        ct.rebuildClassFile();
        
        ct.toBytecode();

        Class c = cl.loadClass(DataClass.class.getCanonicalName());
        Object cla = c.newInstance();
        DataClass c2 = new DataClass();
        
        System.out.println(ReflectionToStringBuilder.reflectionToString(cla) + "\n" + ReflectionToStringBuilder.reflectionToString(c2));
    }
    
    public static void main(String[] args)
    {
        try
        {
            Class clazz = ClassManipulator.newInstance().CreateNewClass("Blafasel").AddField("Name", String.class).AddField("Value", Integer.class).getGeneratedClass();
            Object obj = clazz.newInstance();
            clazz.getDeclaredField("Name").set(obj, "Hans");
            clazz.getDeclaredField("Value").set(obj, 4711);
            System.out.println(ReflectionToStringBuilder.reflectionToString(obj));
            
        }
        catch(Exception ex)
        {
            System.err.println("Fehler:" + ex);
        }
    }
    
}
