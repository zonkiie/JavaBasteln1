package Dijkstra_Fernuni_Aufgabe;

import java.lang.*;
import java.util.*;

/**
 *
 * @author zonkiie
 * @see german wikipedia article
 */
public class DijkstraShortestPathVarKey {
    protected HashMap<Object, HashMap<Object, Integer>> DistanceMatrix = null;
    protected Integer StartingPoint = null;
    protected Integer TargetPoint = null;
    /** Use a very lower Value than the real int max value because of int overflows... */
    public final static Integer MAX_VALUE = 100000;
    public DijkstraShortestPathVarKey(HashMap DistanceMatrix) { this.DistanceMatrix = DistanceMatrix; }
    public DijkstraShortestPathVarKey() {}

    public static DijkstraShortestPathVarKey newInstance() { return new DijkstraShortestPathVarKey(); }
    public DijkstraShortestPathVarKey setDistanceMatrix(HashMap DistanceMatrix) { this.DistanceMatrix = DistanceMatrix; return this; }
    public HashMap getDistanceMatrix() { return this.DistanceMatrix; }
    public DijkstraShortestPathVarKey setStartingPoint(Integer StartingPoint) { this.StartingPoint = StartingPoint; return this; }
    public Integer getStartingPoint() { return this.StartingPoint; }
    public DijkstraShortestPathVarKey setTargetPoint(Integer TargetPoint) { this.TargetPoint = TargetPoint; return this; }
    public Integer getTargetPoint() { return this.TargetPoint; }

    private void Init(HashMap<Object, Integer> Distance, HashMap<Object, Object> PreviousNode, Vector<Object> Q) {
        for(Map.Entry<Object, HashMap<Object, Integer>> entry: DistanceMatrix.entrySet())
        {
            Distance.put(entry.getKey(), MAX_VALUE);
            PreviousNode.put(entry.getKey(), null);
            Q.add(entry.getKey());
        }
        Distance.put(this.StartingPoint, 0);
    }

    private void DistanceUpdate(Object u, Object v, HashMap<Object, Integer> Distance, HashMap<Object, Object> PreviousNode) {
        Integer Alternative = Distance.get(u) + this.DistanceMatrix.get(u).get(v);
        if (Alternative < Distance.get(v)) {
            Distance.put(v, Alternative);
            PreviousNode.put(v, u);
        }
    }
    
    private Object Min_Dist(HashMap<Object, Integer> Distance, Vector<Object> Q)
    {
        Integer dist_value = MAX_VALUE;
        Object dist_key = null;
        for(Object Value: Q)
        {
            if(Distance.get(Value) < dist_value)
            {
                dist_value = Distance.get(Value);
                dist_key = Value;
            }
        }
        return dist_key;
    }
    
    /*
     * 
     */
    private HashMap<Object, Object> Dijkstra()
    {
        HashMap<Object, Integer> Distance = new HashMap<>();
        HashMap<Object, Object> PreviousNode = new HashMap<>();
        Vector<Object> Q = new Vector<Object>();
        this.Init(Distance, PreviousNode, Q);
        while(Q.size() > 0)
        {
            Object u = this.Min_Dist(Distance, Q);
            if(u == null) break;
            Q.removeElement(u);
            for(Object v: Q)
            {
                if(v.equals(u)) continue;
                this.DistanceUpdate(u, v, Distance, PreviousNode);
            }
        }
        return PreviousNode;
    }
    
    /*
     * 
     */
    
    private Vector<Object> CreateShortestPath(HashMap<Object, Object> PreviousNode)
    {
        Vector<Object> Path = new Vector<Object>();
        Path.add(TargetPoint);
        Object u = this.TargetPoint;
        Object v = null;
        while((v = PreviousNode.get(u)) != null)
        {
            u = PreviousNode.get(u);
            Path.add(0, u);
        }
        return Path;
    }

    public Vector<Object> FindShortestPath() {
        HashMap<Object, Object> PreviousNode = this.Dijkstra();
        Vector<Object> ShortestPath = this.CreateShortestPath(PreviousNode);
        return ShortestPath;
    }

    
}
