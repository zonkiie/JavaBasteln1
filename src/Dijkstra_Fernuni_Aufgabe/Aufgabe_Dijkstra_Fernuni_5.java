package Dijkstra_Fernuni_Aufgabe;

import Dijkstra_Fernuni_Aufgabe.DijkstraShortestPathVarKey;
import java.lang.*;
import java.util.*;

/**
 *
 * @author rainer
 */
public class Aufgabe_Dijkstra_Fernuni_5 {

    public enum Node {

        A, B, C, D, E, F, G, H, I
    }

    public static void main(String[] args) {
        /*
         Distance
         F/T A    B    C    D    E    F    G    H    I
         A   0    1    4    7    -    -    -    -    -
         B   -    0    -    3    -    -    9    8    -
         C   -    -    0    1    5    -    -    -    -
         D   -    -    -    0    3    -    -    4    -
         E   -    -    -    -    0    3    -    -    -
         F   -    -    1    -    3    0    -    -    3
         G   -    -    -    -    -    -    0    -    -
         H   -    -    -    -    1    -    3    0    -
         I   -    -    -    -    -    -    -    5    0
        
         */

        //Integer A = 0, B = 1, C = 2, D = 3, E = 4, F = 5, G = 6, H = 7, I = 8;
        HashMap<Node, HashMap<Node, Integer>> Graph = new HashMap<Node, HashMap<Node, Integer>>() {
            {
                put(Node.A, new HashMap<Node, Integer>() {
                    {
                        put(Node.A, 0);
                        put(Node.B, 1);
                        put(Node.C, 4);
                        put(Node.D, 7);
                        put(Node.E, Integer.MAX_VALUE);
                        put(Node.F, Integer.MAX_VALUE);
                        put(Node.G, Integer.MAX_VALUE);
                        put(Node.H, Integer.MAX_VALUE);
                        put(Node.I, Integer.MAX_VALUE);
                    }
                });
                put(Node.B, new HashMap<Node, Integer>() {
                    {
                        put(Node.A, Integer.MAX_VALUE);
                        put(Node.B, 0);
                        put(Node.C, Integer.MAX_VALUE);
                        put(Node.D, 3);
                        put(Node.E, Integer.MAX_VALUE);
                        put(Node.F, Integer.MAX_VALUE);
                        put(Node.G, 9);
                        put(Node.H, 8);
                        put(Node.I, Integer.MAX_VALUE);
                    }
                });
                put(Node.C, new HashMap<Node, Integer>() {
                    {
                        put(Node.A, Integer.MAX_VALUE);
                        put(Node.B, Integer.MAX_VALUE);
                        put(Node.C, 0);
                        put(Node.D, 1);
                        put(Node.E, 5);
                        put(Node.F, Integer.MAX_VALUE);
                        put(Node.G, Integer.MAX_VALUE);
                        put(Node.H, Integer.MAX_VALUE);
                        put(Node.I, Integer.MAX_VALUE);
                    }
                });
                put(Node.D, new HashMap<Node, Integer>() {
                    {
                        put(Node.A, Integer.MAX_VALUE);
                        put(Node.B, Integer.MAX_VALUE);
                        put(Node.C, Integer.MAX_VALUE);
                        put(Node.D, 0);
                        put(Node.E, 3);
                        put(Node.F, Integer.MAX_VALUE);
                        put(Node.G, Integer.MAX_VALUE);
                        put(Node.H, 4);
                        put(Node.I, Integer.MAX_VALUE);
                    }
                });
                put(Node.E, new HashMap<Node, Integer>() {
                    {
                        put(Node.A, Integer.MAX_VALUE);
                        put(Node.B, Integer.MAX_VALUE);
                        put(Node.C, Integer.MAX_VALUE);
                        put(Node.D, Integer.MAX_VALUE);
                        put(Node.E, 0);
                        put(Node.F, 3);
                        put(Node.G, Integer.MAX_VALUE);
                        put(Node.H, Integer.MAX_VALUE);
                        put(Node.I, Integer.MAX_VALUE);
                    }
                });
                put(Node.F, new HashMap<Node, Integer>() {
                    {
                        put(Node.A, Integer.MAX_VALUE);
                        put(Node.B, Integer.MAX_VALUE);
                        put(Node.C, 1);
                        put(Node.D, Integer.MAX_VALUE);
                        put(Node.E, 3);
                        put(Node.F, 0);
                        put(Node.G, Integer.MAX_VALUE);
                        put(Node.H, Integer.MAX_VALUE);
                        put(Node.I, 3);
                    }
                });
                put(Node.G, new HashMap<Node, Integer>() {
                    {
                        put(Node.A, Integer.MAX_VALUE);
                        put(Node.B, Integer.MAX_VALUE);
                        put(Node.C, Integer.MAX_VALUE);
                        put(Node.D, Integer.MAX_VALUE);
                        put(Node.E, Integer.MAX_VALUE);
                        put(Node.F, Integer.MAX_VALUE);
                        put(Node.G, 0);
                        put(Node.H, Integer.MAX_VALUE);
                        put(Node.I, Integer.MAX_VALUE);
                    }
                });
                put(Node.H, new HashMap<Node, Integer>() {
                    {
                        put(Node.A, Integer.MAX_VALUE);
                        put(Node.B, 0);
                        put(Node.C, Integer.MAX_VALUE);
                        put(Node.D, 3);
                        put(Node.E, 1);
                        put(Node.F, Integer.MAX_VALUE);
                        put(Node.G, 3);
                        put(Node.H, 0);
                        put(Node.I, Integer.MAX_VALUE);
                    }
                });
                put(Node.I, new HashMap<Node, Integer>() {
                    {
                        put(Node.A, Integer.MAX_VALUE);
                        put(Node.B, Integer.MAX_VALUE);
                        put(Node.C, Integer.MAX_VALUE);
                        put(Node.D, Integer.MAX_VALUE);
                        put(Node.E, Integer.MAX_VALUE);
                        put(Node.F, Integer.MAX_VALUE);
                        put(Node.G, Integer.MAX_VALUE);
                        put(Node.H, 5);
                        put(Node.I, 0);
                    }
                });
            }
        };

        DijkstraShortestPathVarKey Pathfinder = new DijkstraShortestPathVarKey(Graph);
        for (int i = 0; i <= 8; i++) {
            Pathfinder.setStartingPoint(0).setTargetPoint(i);
            Vector<?> Path = Pathfinder.FindShortestPath();
            System.out.println("Path from A to " + (char) ((int) 'A' + i));
            //System.out.println("Matrix:" + Graph);
            System.out.println("Result:" + Path);
        }
    }
}
