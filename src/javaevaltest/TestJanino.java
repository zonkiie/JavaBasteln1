package javaevaltest;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.codehaus.janino.*;
import org.reflections.Reflections;

/**
 *
 * @author rainer
 * @link http://www.sven-ehrke.de/java_janino_intro.html
 * @link https://today.java.net/pub/a/today/2007/02/15/tackling-performance-problems-with-janino.html
 */
public class TestJanino {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException
    {
        File[] srcDirs = new File[]{new File("/home/rainer/java-sonstiges")};
        String encoding = null;
        ClassLoader parentClassLoader = TestJanino.class.getClassLoader();
        ClassLoader cl = new JavaSourceClassLoader(parentClassLoader, srcDirs, encoding);
        
        Object o1 = cl.loadClass("janino1.janinotest").newInstance();
        Boolean newInstanceExists = false;
        Method[] methods = o1.getClass().getMethods();
        for(Method m: methods)
        {
            System.out.println("Method:" + m.getName());
            if(m.getName().equals("newInstance")) newInstanceExists = true;
            //System.out.println("Invoke:" + m.invoke(o1));
        }
        if(newInstanceExists)
        {
            Object o2 = o1.getClass().getMethod("newInstance").invoke(o1);
            Method m11 = o2.getClass().getMethod("m11");
            m11.invoke(o2);
        }
    }
}
