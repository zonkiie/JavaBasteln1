/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javaevaltest;

import javax.script.*;

/**
 *
 * @author rainer
 */
public class JavaEvalTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ScriptException {
        // TODO code application logic here
        // create a script engine manager
        ScriptEngineManager factory = new ScriptEngineManager();
        // create a JavaScript engine
        ScriptEngine engine = factory.getEngineByName("JavaScript");
        // evaluate JavaScript code from String
        
        engine.eval("var b = eval('true && !false && 5 < 3')");
        Boolean b =(Boolean) engine.get("b");

        System.out.println(b);
        engine.eval("print(\"Hallo\\n\");");
    }
    
}
