package Pattern;

import java.lang.*;
import java.io.*;
import java.util.*;

interface IStrategy {
    public void execute(Data data);
}

class Strategy_1 implements IStrategy {

    @Override
    public void execute(Data data) {
        data.d1 = "Data1";
        System.out.println(this.getClass().getCanonicalName() + ":" + data);
    }
    
}

class Strategy_2 implements IStrategy {

    @Override
    public void execute(Data data) {
        data.d2 = "Data2";
        System.out.println(this.getClass().getCanonicalName() + ":" + data);
    }

}

/**
 *
 * @author rainer
 */
public class Strategy {
    public static void main(String [] args)
    {
        Strategy str = new Strategy();
        Data data = new Data();
        str.handle(data, new Strategy_1());
        data = new Data();
        str.handle(data, new ArrayList<IStrategy>(){{ add(new Strategy_1()); add(new Strategy_2()); }});
    }
    
    public void handle(Data data, IStrategy strategy)
    {
        handle(data, new ArrayList<IStrategy>(){{ add(strategy); }});
    }
    
    public void handle(Data data, List<IStrategy> strategies)
    {
        for(IStrategy istr: strategies) {
            istr.execute(data);
        }
    }
}
